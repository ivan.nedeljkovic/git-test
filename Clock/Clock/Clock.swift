//
//  Clock.swift
//  Clock
//
//  Created by Ivan Nedeljkovic on 13/02/2018.
//  Copyright © 2018 Ivan Nedeljkovic. All rights reserved.
//

import UIKit

class Clock: CAShapeLayer {
    
    private var hourLayer: CAShapeLayer?
    private var minuteLayer: CAShapeLayer?
    
    var time: Date? {
        didSet {
            let calendar = Calendar(identifier: .gregorian)
            let components = calendar.dateComponents([.hour, .minute], from: time!)
            
            let hourRotationAngle = CGFloat(components.hour!)/12.0 * 2.0 * CGFloat.pi
            let hourRotationTransformation = CGAffineTransform(rotationAngle: hourRotationAngle)
            hourLayer?.setAffineTransform(hourRotationTransformation)
            
            let minuteRotationAngle = CGFloat(components.minute!)/60.0 * 2.0 * CGFloat.pi
            let minuteRotationTransformation = CGAffineTransform(rotationAngle: minuteRotationAngle)
            minuteLayer?.setAffineTransform(minuteRotationTransformation)
        }
    }
    
    override init() {
        super.init()
        
        self.bounds = CGRect(x: 0, y: 0, width: 200, height: 200)
        self.path = UIBezierPath(ovalIn: self.bounds).cgPath
        self.fillColor = UIColor.lightGray.cgColor
        self.strokeColor = UIColor.red.cgColor
        self.lineWidth = 4
        
        hourLayer = CAShapeLayer()
        hourLayer!.path = UIBezierPath(rect: CGRect(x: -2, y: -70, width: 4, height: 70)).cgPath
        hourLayer!.fillColor = UIColor.darkGray.cgColor
        hourLayer!.position = CGPoint(x: bounds.width/2, y: bounds.height/2)
        self.addSublayer(hourLayer!)
        
        minuteLayer = CAShapeLayer()
        minuteLayer!.path = UIBezierPath(rect: CGRect(x: -1, y: -90, width: 2, height: 90)).cgPath
        minuteLayer!.fillColor = UIColor.darkGray.cgColor
        minuteLayer!.position = CGPoint(x: bounds.width/2, y: bounds.height/2)
        self.addSublayer(minuteLayer!)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
