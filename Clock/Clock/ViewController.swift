//
//  ViewController.swift
//  Clock
//
//  Created by Ivan Nedeljkovic on 13/02/2018.
//  Copyright © 2018 Ivan Nedeljkovic. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var clockFace: Clock?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        clockFace = Clock()
        clockFace?.position = CGPoint(x: view.bounds.width/2, y: 200)
        view.layer.addSublayer(clockFace!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func timePickerDidChangeValue(sender: UIDatePicker) {
        clockFace?.time = sender.date
    }
}

